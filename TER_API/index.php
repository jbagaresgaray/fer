<?php
session_start();
include('accounts_api.php');
include('subject_api.php');
include('evaluation_api.php');
include('dean_api.php');
include('faculty_api.php');
include('peer_evaluation.php');
include('dean_evaluation.php');
include('chairman_evaluation.php');

header("Content-Type: application/json");

switch ($_POST['command']) {
	case "settings":
		get_year_sem_userid();
		break;
	case "login": 
		login($_POST['username'], $_POST['password'],$_POST['usertype']);
		break;
	case "profile":
		getsubject_enroll($_POST['currentyear'],$_POST['currentsemester'],$_POST['myid']);
		break;
	case "checker":
		checker($_POST['offercode'],$_POST['subject'],$_POST['myid']);
		break;
	case "checkerdean":
		checkerdean($_POST['myid'],$_POST['faculty_id']);
		break;
	case "checkerpeer":
		checkerpeer($_POST['myid'],$_POST['faculty_id']);
		break;
	case "checkerchairman":
		checkerchairman($_POST['myid'],$_POST['faculty_id']);
		break;
	case "faculty_info":
		get_instructor_info($_POST['offercode']);
		break;
	case "generate_question":
		generate_question($_POST['ter_scale']);
		break;
	case "save_evaluation":
		submit_evaluation($_POST['faculty_id'],$_POST['ocode'],$_POST['comments'],$_POST['currentuserid'],$_POST['ter_scale'],$_POST['data']);
		break;
	case "getcollegebyid":
		getcollegebyid($_POST['idnumber']);
		break;
	case "dean_departments":
		 getdepartments2($_POST['college']);
		 break;
	case "dean_faculty":
		getfaculty_2($_POST['department'],$_POST['idnumber']);
		break;
	case "getdepartmentdean":
		getdepartmentdean($_POST['department'],$_POST['idnumber']);
		break;
	case "getdepartmentfaculty":
		getdepartmentfaculty($_POST['department'],$_POST['idnumber']);
		break;
	case "peerlist":
		getfacultypeerlist($_POST['id']);
		break;
    case "get_instructor_subjects":
        getfacsubjects2_3($_POST['insidnumber'],$_POST['ter_scale']);
        break;
	case "getdepartment":
		getfacultyfaculty($_POST['id']);
		break;
	case "getfacultybyid":
		getfacultybyid($_POST['facidnum']);
		break;
	case "getfacultybyid":
		getfacultybyid($_POST['peer_idnumber']);
		break;
	case "generate_question_peer":
		generate_question_peer($_POST['ter_scale']);
		break;
	case "generate_question_dean":
		generate_question_dean($_POST['ter_scale']);
		break;
	case "generate_question_chairman":
		generate_question_chairman($_POST['ter_scale']);
		break;
	case "submit_evaluation_peer":
		submit_evaluation_peer($_POST['faculty_id'],$_POST['comments'],$_POST['currentuserid'],$_POST['ter_scale'],$_POST['data']);
		break;
	case "submit_evaluation_dean":
		submit_evaluation_dean($_POST['faculty_id'],$_POST['comments'],$_POST['currentuserid'],$_POST['ter_scale'],$_POST['data']);
		break;
	case "submit_evaluation_chairman":
		submit_evaluation_chairman($_POST['faculty_id'],$_POST['comments'],$_POST['currentuserid'],$_POST['ter_scale'],$_POST['data']);
		break;

}
exit();
?>
