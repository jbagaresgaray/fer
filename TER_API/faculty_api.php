<?php

function getfacultypeerlist($id) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = " SELECT distinct p.IDNUMBER, p.FULLNAME FROM SELECT_PEERLIS($id) p";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $data = array();
    $data2 = array();
    while ($row = ibase_fetch_row($result)) {
        $idnumber = $row[0];
        $fullname = $row[1];

        $highlight = checkpeerb4($id, $idnumber);

        $data = array('idnumber' => $idnumber, 'fullname' => $fullname, 'highlight' => $highlight);
        array_push($data2, $data);
    }
    print json_encode(array('success' => true, 'peerlist' => $data2));
}


function getfacsubjects2_3($currentid, $ter_id) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $autoresult = 0;
    $query = "select * from GETFACSUBJECTS($currentid)";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $count = 0;
    $finalresult = 0;
    $data = array();
    $data2 = array();
    while ($row = ibase_fetch_row($result)) {
        $subject = $row[0];
        $section = $row[1];
        $desc = $row[2];
        $offercode = getoffercode($subject, $section);
        $finalresult += autofetchreports($subject, $section, $ter_id);
        $autoresult = "    ".autofetchreports($subject, $section, $ter_id);


        $data = array('subject' => $subject,
            'section' => $subject,
            'desc' => $desc,
            'result' => (ROUND($autoresult, 2).
                " %")
        );
        // $data = array('subject'=>$subject,
        //              'section' => $subject,
        //              'desc'=>$desc
        //              );

        array_push($data2, $data);
        $count++;
    }
    if ($count > 0) {
        $finalresult /= $count;
    } else {
        $finalresult = 0;
    }
    print json_encode(array('success' => true, 'subjects' => $data2, 'finalresult' => round($finalresult, 2)));
}


function checkpeerb4($myid, $evaluatee) {
    $cnt = 0;
    $isdone = false;
    $myID = $myid;
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "select * from CHECKIFEXISTFACULTY WHERE IDNUMBER=$myid AND EVALUATEE=$evaluatee";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    while ($row = ibase_fetch_row($result)) {
        $id = $row[0];
        $code = $row[1];
        if ($myID == $id && $evaluatee == $code) {
            $cnt++;
        }
    }
    if ($cnt > 0) {
        $isdone = true;
    } else {
        $isdone = false;
    }

    return $isdone;
}

function checkerpeer($myid, $evaluatee) {
    $uu = 0;
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "SELECT * FROM CHECKIFEXISTFACULTY WHERE IDNUMBER=$myid AND EVALUATEE=$evaluatee";
    $arr = array();
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    if ($row = ibase_fetch_row($result)) {
        print json_encode(array('success' => false, 'msg' => 'You already rated this instructor. Please choose the next one..'));
    } else {
        print json_encode(array('success' => true, 'msg' => ''));
    }
    /*		while($row = ibase_fetch_row($result)){
			$id = $row[0];
			$code = $row[1];
			if($myid==$id  && $evaluatee == $code){
				$uu++;
			}
		}
	    if($uu>0){

		}else{

		}*/
}

function autofetchreports($subject, $section, $ter_id) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");

    $qcount = getqcount($ter_id);

    for ($qnum = 1; $qnum <= $qcount; $qnum++) {

        $c1 = 5;
        $c2 = 6;
        $c3 = 7;
        $c4 = 8;
        $c5 = 9;
        $c6 = 10;

        $query0 = "execute procedure GETQUESTION($qnum)";
        $result0 = ibase_query($connect, $query0) or die(ibase_errmsg());
        $row0 = ibase_fetch_row($result0);
        $_session['question'.$qnum] = $row0[0];

        $query1 = "execute procedure GETSUMS($qnum,$c1,'$subject','$section')";
        $result1 = ibase_query($query1) or die(ibase_errmsg());
        $row1 = ibase_fetch_row($result1);

        if ($row1[0] != null || $row1[0] != '') {
            $_session['a'.$qnum] = $row1[0];
        } else {
            $_session['a'.$qnum] = 0;
        }

        $query2 = "execute procedure GETSUMS($qnum,$c2,'$subject','$section')";
        $result2 = ibase_query($connect, $query2) or die(ibase_errmsg());
        $row2 = ibase_fetch_row($result2);

        if ($row2[0] != null || $row2[0] != '') {
            $_session['b'.$qnum] = $row2[0];
        } else {
            $_session['b'.$qnum] = 0;
        }

        $query3 = "execute procedure GETSUMS($qnum,$c3,'$subject','$section')";
        $result3 = ibase_query($connect, $query3) or die(ibase_errmsg());
        $row3 = ibase_fetch_row($result3);

        if ($row3[0] != null || $row3[0] != '') {
            $_session['c'.$qnum] = $row3[0];
        } else {
            $_session['c'.$qnum] = 0;
        }

        $query4 = "execute procedure GETSUMS($qnum,$c4,'$subject','$section')";
        $result4 = ibase_query($connect, $query4) or die(ibase_errmsg());
        $row4 = ibase_fetch_row($result4);

        if ($row4[0] != null || $row4[0] != '') {
            $_session['d'.$qnum] = $row4[0];
        } else {
            $_session['d'.$qnum] = 0;
        }

        $query5 = "execute procedure GETSUMS($qnum,$c5,'$subject','$section')";
        $result5 = ibase_query($connect, $query5) or die(ibase_errmsg());
        $row5 = ibase_fetch_row($result5);

        if ($row5[0] != null || $row5[0] != '') {
            $_session['e'.$qnum] = $row5[0];
        } else {
            $_session['e'.$qnum] = 0;
        }

        $query6 = "execute procedure GETSUMS($qnum,$c6,'$subject','$section')";
        $result6 = ibase_query($connect, $query6) or die(ibase_errmsg());
        $row6 = ibase_fetch_row($result6);

        if ($row6[0] != null || $row6[0] != '') {
            $_session['f'.$qnum] = $row6[0];
        } else {
            $_session['f'.$qnum] = 0;
        }



        $_session['sum'.$qnum] = ($_session['a'.$qnum] + $_session['b'.$qnum] + $_session['c'.$qnum] + $_session['d'.$qnum] + $_session['e'.$qnum] + $_session['f'.$qnum]);

        $a = $_session['a'.$qnum];
        $b = $_session['b'.$qnum];
        $c = $_session['c'.$qnum];
        $d = $_session['d'.$qnum];
        $e = $_session['e'.$qnum];
        $f = $_session['f'.$qnum];

        if ($_session['sum'.$qnum] > 0) {
            $_session['average'.$qnum] = (($a * 5) + ($b * 6) + ($c * 7) + ($d * 8) + ($e * 9) + ($f * 10)) / $_session['sum'.$qnum];
        } else {

            $_session['average'.$qnum] = 0;
        }

    }


    $_session['finalsum1'] = 0;
    $_session['finalsum2'] = 0;
    $_session['finalsum3'] = 0;
    $_session['finalsum4'] = 0;
    $_session['finalsum5'] = 0;
    $_session['finalsum6'] = 0;

    for ($k = 1; $k <= $qcount; $k++) {
        if ($k >= 1 && $k <= 4) {
            $_session['finalsum1'] += $_session['average'.$k];
            if ($k == 4) {
                $_session['part1result'] = ($_session['finalsum1'] * 6.250) * 0.30;
            }
        } else if ($k >= 5 && $k <= 9) {
            $_session['finalsum2'] += $_session['average'.$k];

            if ($k == 9) {
                $_session['part2result'] = ($_session['finalsum2'] * 5.000) * 0.15;
            }
        } else if ($k >= 10 && $k <= 17) {
            $_session['finalsum3'] += $_session['average'.$k];

            if ($k == 17) {
                $_session['part3result'] = ($_session['finalsum3'] * 3.125) * 0.40;
            }
        } else if ($k >= 18) {
            $_session['finalsum4'] += $_session['average'.$k];

            if ($k == 20) {
                $_session['part4result'] = ($_session['finalsum4'] * 8.333) * 0.15;
            }
        }


    }
    $final = $_session['part1result'] + $_session['part2result'] + $_session['part3result'] + $_session['part4result'];

    //echo round($final,2);
    return $final;

}

?>
