<?php
// echo 'evaluation';

function getnegativequestion_peer($number,$ter_id){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$isempty = "";
	$kissai = 0;
	$query="SELECT a.ORDE_R FROM TERQUESTIONSREV a where a.POSITIVE = '0' and a.ORDE_R = $number AND a.TERID=$ter_id";
	$result=ibase_query($connect,$query) or die(ibase_errmsg()) ;
	while($row = ibase_fetch_row($result)){
		$kissai++;
	}
    if($kissai>0){
		$isempty = "*";
	}else{
		$isempty = "";
	}
	return $isempty;
}

function generate_question_peer($ter_id){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$query="SELECT b.ORDE_R,
		CAST(SUBSTRING(b.QUESTION FROM 1 FOR 32000) AS VARCHAR(32000)) AS QUESTION
		FROM TERQUESTIONSREV b
		INNER JOIN TERSCALE a ON a.ID=b.TERID
		WHERE a.SCALE='2';";
	// $query= "SELECT p.ORDE_R,p.QUESTION FROM SELECT_QUESTIONSFORSCALE('2') p";
	$result=ibase_query($connect,$query) or die(ibase_errmsg()) ;
	$t = 1;
	$y =1;
	$data = array();
	$data2 = array();
	while($row=ibase_fetch_row($result,IBASE_TEXT)){

		$isempty = getnegativequestion_peer($t,$ter_id);

		$data= array('Question' => $t.". ) ".$row[1],'isempty' => $isempty,
			'Never_ID' => "r1".$t,'Never_NAME' =>$row[0],
			'Seldom_ID1' => "r2".$t,'Seldom_NAME1' => $row[0],
			'Seldom_ID2' => "r3".$t, 'Seldom_NAME2' => $row[0],
			'Often_ID1'=>"r4".$t,'Often_NAME1' => $row[0],
			'Often_ID2'=>"r5".$t,'Often_NAME2' => $row[0],
			'Always_ID'=>"r6".$t,'Always_NAME' => $row[0]);

		array_push($data2 ,$data);
		$t++;
	 	$y++;
	}
		print json_encode(array('success' =>true,'instructor' =>$data2));
}

function getevalstartid_peer(){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$query="select max(a.EVALSTARTID) from evalstart a";
	$result=ibase_query($connect,$query) or die(ibase_errmsg()) ;
	$oc = ibase_fetch_row($result);
	$es = $oc[0];
	return $es;
}

function getcurrentevalid_peer(){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
    $query10="execute procedure GETCURRENTEVALID";
	$result=ibase_query($connect,$query10) or die(ibase_errmsg()) ;
	$row=ibase_fetch_row($result);
	$currentevalID 	= $row[0];
	return $currentevalID;
}

function getqcount_peer($ter_id){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
    $query2="execute procedure GETQCOUNT";
	$result2=ibase_query($connect,$query2) or die(ibase_errmsg());
	$row=ibase_fetch_row($result2);
	$qcount = $row[0];
	return $qcount;
}

function addcomment_peer($currentevalID,$comments){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$query2="execute procedure INSERTCOMMENTS($currentevalID,'$comments')";
	$result2=ibase_query($connect,$query2) or die(ibase_errmsg());
}

function InsertEvaluation_peer($EVALUATOR,$EVALUATEE,$DESCRIPTION,$TERTYPE){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$evaltartid = getevalstartid_peer();
//	$last_insert_id = geteval_Last_insert_id();
    $query1="INSERT INTO TEREVALUATIONS(EVALUATOR,EVALUATEE,DESCRIPTION,TERTYPE,DATESTART,DATEDONE,EVALID)
		VALUES ('$EVALUATOR','$EVALUATEE','$DESCRIPTION','$TERTYPE','now','now',$evaltartid);";
	$result1=ibase_query($connect,$query1) or die(ibase_errmsg());
}

function inserttohist_peer($idnumber,$evaluatee,$evalid){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
    $query2="execute procedure INSERTTOPEERHISTORY($idnumber,$evaluatee,$evalid)";
	$result2=ibase_query($connect,$query2) or die(ibase_errmsg());
}

function main_insert_peer($currentevalID,$ter_id,$answer){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
    $query5="EXECUTE PROCEDURE INSERTNEWANSWERS($currentevalID,$ter_id,$answer)";
    $result5=ibase_query($connect,$query5) or die(ibase_errmsg());
}

function submit_evaluation_peer($faculty_id,$comments,$currentuserid,$ter_id,$data){
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	$qcount = getqcount($ter_id);
	$USERID = $currentuserid;
	$answer= array();

	$countforerrors = 0;
	$count = 0;

	foreach($data as $value){
		if($value['value']!='' || $value['value']!=null){
			$selected_radio = $value['value'];
			$qr_no = $value['id'];

			array_push($answer,array('qr_no' => $qr_no,'rating'=> $selected_radio));
			$count++;
		}else{
			$countforerrors++;
		}
	 }

	if($countforerrors==0){
		if($count==$qcount){

		  		InsertEvaluation_peer($USERID,$faculty_id,'',$ter_id);

				$currentevalID = getcurrentevalid();

				$length = count($data);
				for($y=0;$y<$length;$y++){
					main_insert_peer($currentevalID,$answer[$y]['qr_no'],$answer[$y]['rating']);
				}

				addcomment_peer($currentevalID,$comments);
				inserttohist_peer($currentuserid,$faculty_id,$currentevalID);

	        print json_encode(array('success' =>true,'msg' =>'Successfully rated this instructor..Thank you..'));
		}
	}else{
		print json_encode(array('success'=>false,'msg'=>'There are unchecked fields, please fillup those. Thank you..'));
	}

}

?>
