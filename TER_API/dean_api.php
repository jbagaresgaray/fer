<?php

function getdepartmentbyid($id) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "execute procedure getdepartmentbyid($id)";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $data = array();
    while ($row = ibase_fetch_row($result)) {
        $data = array('department' => $row[0]);
    }
    print json_encode(array('success' => true, 'department' => $data));
}

function getcollegebyid($idnumber) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "execute procedure GETCOLLEGEBYID($idnumber)";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $data = array();
    while ($row = ibase_fetch_row($result)) {
        //		$data = array('college' => $row[0]);
        array_push($data, $row[0]);
    }
    print json_encode(array('success' => true, 'college' => $data));

}

function getdepartments2($college) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "SELECT * FROM GETDEPARTMENTS('$college')";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $data = array();
    while ($row = ibase_fetch_row($result)) {
        array_push($data, $row[0]);
    }
    print json_encode(array('success' => true, 'department' => $data));
}

function getfaculty_2($department, $idnumber) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "SELECT a.IDNUMBER,a.LASTNAME,a.FIRSTNAME,n.DEPARTMENT,a.PICTURE FROM FACULTY a INNER JOIN DEPARTMENTCHAIRMAN n ON a.IDNUMBER =n.IDNUMBER WHERE n.DEPARTMENT = '$department'";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $row = ibase_fetch_row($result);
    if ($row) {
        $bl = null;

        if ($row[4]) {
            $blob_data = ibase_blob_info($row[4]);
            $blob_hndl = ibase_blob_open($row[4]);
            $bl = ibase_blob_get($blob_hndl, $blob_data[0]);
        }

        $highlight = checkdeanb4($idnumber, $row[0]);

        $data = array('IDNUMBER' => $row[0],
            'LASTNAME' => $row[1],
            'FIRSTNAME' => $row[2],
            'fullname' => $row[1].
            ", ".$row[2],
            'department' => $row[3],
            'ter_scale' => '50103',
            'highlight' => $highlight,
            'picture' => base64_encode($bl));

        print json_encode(array('success' => true, 'faculty' => $data, 'finalrating1' => 0, ));
    } else {
        errorJson('No record found on the selected item.');
    }
}

function getdepartmentdean($department, $idnumber) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "SELECT a.IDNUMBER,a.LASTNAME,a.FIRSTNAME,n.DEPARTMENT,a.PICTURE FROM FACULTY a INNER JOIN DEPARTMENTDEAN n ON a.IDNUMBER =n.IDNUMBER WHERE n.DEPARTMENT = '$department'";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $row = ibase_fetch_row($result);
    if ($row) {
        $bl = null;

        if ($row[4]) {
            $blob_data = ibase_blob_info($row[4]);
            $blob_hndl = ibase_blob_open($row[4]);
            $bl = ibase_blob_get($blob_hndl, $blob_data[0]);
        }

        $highlight = checkdeanb4($idnumber, $row[0]);

        $data = array('IDNUMBER' => $row[0],
            'LASTNAME' => $row[1],
            'FIRSTNAME' => $row[2],
            'fullname' => $row[1].
            ", ".$row[2],
            'department' => $row[3],
            'ter_scale' => '50103',
            'highlight' => $highlight,
            'picture' => base64_encode($bl));

        print json_encode(array('success' => true, 'faculty' => $data, 'finalrating1' => 0, ));
    } else {
        errorJson('No record found on the selected item.');
    }
}

function getdepartmentfaculty($department) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "select * from GETFACULTY('$department')";
    $query = "SELECT a.IDNUMBER,a.LASTNAME ,a.FIRSTNAME,n.DEPARTMENT,a.PICTURE FROM FACULTY a INNER JOIN DEPARTMENTCHAIRMAN n ON a.IDNUMBER =n.IDNUMBER WHERE n.DEPARTMENT = '$department'";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $row = ibase_fetch_row($result);
    if ($row) {
        $bl = null;

        if ($row[4]) {
            $blob_data = ibase_blob_info($row[4]);
            $blob_hndl = ibase_blob_open($row[4]);
            $bl = ibase_blob_get($blob_hndl, $blob_data[0]);
        }

        $highlight = checkdeanb4($idnumber, $row[1]);

        $data = array('IDNUMBER' => $row[0],
            'LASTNAME' => $row[1],
            'FIRSTNAME' => $row[2],
            'fullname' => $row[1].
            ", ".$row[2],
            'department' => $row[3],
            'ter_scale' => '50103',
            'highlight' => $highlight,
            'picture' => base64_encode($bl));

        print json_encode(array('success' => true, 'faculty' => $data, 'finalrating1' => 0, ));
    } else {
        errorJson('Fetching data failed');
    }
}

function getfacultybyid($id) {
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "execute procedure GETFACULTYBYID($id)";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    $numofrow = ibase_num_fields($result);
    $data = array();
    while ($row = ibase_fetch_row($result)) {
        $bl = null;

        if ($row[2]) {
            $blob_data = ibase_blob_info($row[2]);
            $blob_hndl = ibase_blob_open($row[2]);
            $bl = ibase_blob_get($blob_hndl, $blob_data[0]);
        }
        $fullname = $row[0];
        $data = array('fullname' => $row[0], 'picture' => base64_encode($bl));
    }
    print json_encode(array('success' => true, 'facultyname' => $data));
}

function checkdeanb4($myid, $evaluatee) {
    $cnt = 0;
    $isdone = false;
    $myID = $myid;
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "select * from CHECKIFEXISTDEAN WHERE IDNUMBER=$myid AND EVALUATEE=$evaluatee";
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    while ($row = ibase_fetch_row($result)) {
        $id = $row[0];
        $code = $row[1];
        if ($myID == $id && $evaluatee == $code) {
            $cnt++;
        }
    }
    if ($cnt > 0) {
        $isdone = true;
    } else {
        $isdone = false;
    }

    return $isdone;
}

function checkerdean($myid, $evaluatee) {
    $uu = 0;
    $connect = ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB", "sysdba", "masterkey");
    $query = "SELECT * FROM CHECKIFEXISTDEAN WHERE IDNUMBER=$myid AND EVALUATEE=$evaluatee";
    $arr = array();
    $result = ibase_query($connect, $query) or die(ibase_errmsg());
    if ($row = ibase_fetch_row($result)) {
        print json_encode(array('success' => false, 'msg' => 'You already rated this dean. Please choose the next one..'));
    } else {
        print json_encode(array('success' => true, 'msg' => ''));
    }
}


?>
