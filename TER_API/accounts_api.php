<?php
//API implementation to come here
// echo 'accounts';

function errorJson($msg){
	print json_encode(array('success'=>false,'error'=>$msg));
	exit();
}

function login($idnumber, $password, $usertype) {
	$connect=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");

 	if($usertype== "STUDENT"){
		$query = "SELECT a.IDNUMBER,a.LASTNAME ,a.FIRSTNAME,A.COURSE,A.CURRICULUM,A.GENDER,B.PASS_WORD from students a INNER JOIN STUDENTACCOUNTS B ON a.IDNUMBER = B.IDNUMBER WHERE A.IDNUMBER =$idnumber AND B.PASS_WORD =$password";
		$data = array();
		// $result=ibase_query($connect,$query);
		$result=ibase_query($connect,$query) or die(ibase_errmsg());
		$row = ibase_fetch_row($result);
		if($row){
			$dhan = 0;
			$data= array('IDNUMBER' => $row[0],
			'LASTNAME' => $row[1],
			'FIRSTNAME' => $row[2], 
			'COURSE'=> $row[3],
			'CURRICULUM'=> $row[4],
			'GENDER'=> $row[5],
			'PASS_WORD' => $row[6],
			'myid' => $row[0],
			'studfullname' => $row[1].", ".$row[2],
			'initialnum' => $dhan,
			'indicator' => '0',
			'indicator1' => '0',
			'indicator2' => '0',
			'indicator3' => '1',
			'indicator4' => '0',
            'ter_scale'=>'50101');

 			print json_encode(array('success' =>true,'users' =>$data ,'type'=>'student'));
		}else{
			errorJson('Authorization failed');
		}
	
	}else if($usertype== "FACULTY"){
		$query = "SELECT a.IDNUMBER,a.LASTNAME ,a.FIRSTNAME,n.PASS_WORD,a.PICTURE from FACULTY a INNER JOIN FACULTYACCOUNTS n ON a.IDNUMBER = n.IDNUMBER where a.IDNUMBER = $idnumber and n.PASS_WORD = $password";
		$data = array();
		// $result=ibase_query($connect,$query);
		$result=ibase_query($connect,$query) or die(ibase_errmsg());
		$row = ibase_fetch_row($result);
		if($row){
			$dhan = 0;
			$bl = null;

			if($row[4]){
				$blob_data = ibase_blob_info($row[4]);
				$blob_hndl = ibase_blob_open($row[4]);
				$bl = ibase_blob_get($blob_hndl, $blob_data[0]);
			}

			$data= array('IDNUMBER' => $row[0],
			'LASTNAME' => $row[1],
			'FIRSTNAME' => $row[2],
			'instructorfullname' => $row[1].", ".$row[2],
			'insidnumber' => $row[0],
			'initialnum' => $dhan,
			'indicator' => '0',
			'indicator1' => '0',
			'indicator2' => '1',
			'indicator3' => '0',
			'indicator4' => '0',
            'ter_scale'=>'50102',
			'picture' =>base64_encode($bl));

 			print json_encode(array('success' =>true,'users' =>$data,'type'=>'faculty'));
		}else{
			errorJson('Authorization failed');
		}

	}else if($usertype== "DEAN"){
		$query = "SELECT a.IDNUMBER, a.LASTNAME ,a.FIRSTNAME,n.PASS_WORD,a.PICTURE from FACULTY a INNER JOIN FACULTYACCOUNTS n ON a.IDNUMBER =n.IDNUMBER where a.IDNUMBER = $idnumber and n.PASS_WORD = $password AND n.MODULE = 'LEVEL1'";
		$data = array();
		// $result=ibase_query($connect,$query);
		$result=ibase_query($connect,$query) or die(ibase_errmsg());
		$row = ibase_fetch_row($result);
		if($row){
			$dhan = 0;
			$bl = null;

			if($row[4]){
				$blob_data = ibase_blob_info($row[4]);
				$blob_hndl = ibase_blob_open($row[4]);
				$bl = ibase_blob_get($blob_hndl, $blob_data[0]);
			}

			$data= array('IDNUMBER' => $row[0],
			'LASTNAME' => $row[1],
			'FIRSTNAME' => $row[2], 
			'deanfullname' => $row[1].", ".$row[2],
			'deanidnumber' => $row[0],
			'initialnum' => $dhan,
			'indicator' => '0',
			'indicator1' => '1',
			'indicator2' => '0',
			'indicator3' => '0',
			'indicator4' => '0',
            'ter_scale'=>'50103',
			'picture' =>base64_encode($bl));

 			print json_encode(array('success' =>true,'users' =>$data,'type'=>'dean'));
		}else{
			errorJson('Authorization failed');
		}

	}else if($usertype == "CHAIRPERSON"){
		$query="SELECT a.IDNUMBER,a.LASTNAME,a.FIRSTNAME,n.PASS_WORD,a.PICTURE from FACULTY a INNER JOIN FACULTYACCOUNTS n ON a.IDNUMBER =n.IDNUMBER where a.IDNUMBER = $idnumber and n.PASS_WORD = $password AND n.MODULE = 'LEVEL2'";
		$result=ibase_query($connect,$query) or die(ibase_errmsg());
		$row = ibase_fetch_row($result);
		if($row){
			$dhan = 0;
			$bl = null;

			if($row[4]){
				$blob_data = ibase_blob_info($row[4]);
				$blob_hndl = ibase_blob_open($row[4]);
				$bl = ibase_blob_get($blob_hndl, $blob_data[0]);
			}

			$data= array('IDNUMBER' => $row[0],
			'LASTNAME' => $row[1],
			'FIRSTNAME' => $row[2],
			'fullname' => $row[1].", ".$row[2],
			'initialnum' => $dhan,
			'myid2' =>$row[0],
			'indicator' => '0',
			'indicator1' => '1',
			'indicator2' => '0',
			'indicator3' => '0',
			'indicator4' => '0',
            'ter_scale'=>'50103',
			'picture' =>base64_encode($bl));

 			print json_encode(array('success' =>true,'users' =>$data,'type'=>'chairperson'));
		}else{
			errorJson('Authorization failed');
		}

	}


	
}


?>
