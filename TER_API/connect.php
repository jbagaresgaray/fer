<?php
	
	$link=ibase_connect("localhost:C:\wamp\www\MyTER\TER_API\MSUNTER.FDB","sysdba","masterkey");
	
	function query() {
		global $link;
		$debug = false;
		
		//get the sql query
		$args = func_get_args();
		$sql = array_shift($args);

		//secure the input
		for ($i=0;$i<count($args);$i++) {
			$args[$i] = urldecode($args[$i]);
			// $args[$i] = mysqli_real_escape_string($link, $args[$i]);
			$args[$i] = str_replace("'", "''", $args[$i]);
		}
		
		//build the final query
		$sql = vsprintf($sql, $args);
		
		if ($debug) print $sql;
		
		//execute and fetch the results
		$result = ibase_query($link, $sql);
		if ($result) {
			
			$rows = array();

			if ($result!== true)
			while ($d = ibase_fetch_assoc($result)) {
				array_push($rows,$d);
			}
			
			//return json
			return array('success'=> true,'result'=>$rows);
			
		} else {
		
			//error
			return array('error'=>'Database error');
		}
	}


?>