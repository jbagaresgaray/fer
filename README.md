FER (Faculty Evaluation Report System)
==========================================

Faculty Evaluation Report System

![Faculty-Evaluation-Report-System](http://oi59.tinypic.com/28hitl5.jpg)

![Faculty-Evaluation-Report-System](http://oi60.tinypic.com/157hffd.jpg)

## Demo
Coming soon.

## Versions

We are going to use master branch to keep up to date with Mobile versions. Previous versions will be moved to corresponding branches.

Enhance User Interface for better user experience.

Update the from the latest Jquery Mobile(1.4.2) and uses the latest [nativeDroid v0.2.7 ](http://nativedroid.godesign.ch/)

## Usage

This is use to evaluate performance of the faculty or professor.
-- 1. Student as Raters
-- 2. Peer as Raters
-- 3. Chairpersons/ Deans as Raters


## Contributor(s)

* [@jbagaresgaray](https://github.com/jbagaresgaray)


##License:

None, (Capstone Project)