document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("backbutton", onBackButton, false);

$(document).ready(function() {
    console.log('Student Profile');

    var redirect = false;
    var indicator = window.localStorage.getItem("indicator");
    var indicator1 = window.localStorage.getItem("indicator1");
    var indicator2 = window.localStorage.getItem("indicator2");
    var indicator3 = window.localStorage.getItem("indicator3");
    var indicator4 = window.localStorage.getItem("indicator4");

    if (indicator != '1') {
        redirect = true;
        console.log('indicator redirect = true');
    } else if (indicator1 != '1') {
        redirect = true;
        console.log('indicator1 redirect = true');
    } else if (indicator2 != '1') {
        redirect = true;
        console.log('indicator2 redirect = true');
    } else if (indicator3 != '1') {
        redirect = true;
        console.log('indicator3 redirect = true');
    } else if (indicator4 != '1') {
        redirect = true;
        console.log('indicator4 redirect = true');
    } else {
        redirect = false
        console.log('redirect = false');
    }

    // if (redirect) {
    //     window.location.href = "index.html";
    // } else {
    onDeviceReady();
    // }

});

function onBackButton() {
    alert("Back Button fired");
}


function onDeviceReady() {
    student_profile();
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.html";
}

function student_profile() {
    $.mobile.loading("show");
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'profile',
            myid: window.localStorage.getItem("myID"),
            currentsemester: window.localStorage.getItem("currentsemester"),
            currentyear: window.localStorage.getItem("currentyear")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                $.mobile.loading("hide");
                alert(decode.error);
                window.localStorage.clear();
                return;
            } else {
                console.log(decode.result);
                $('#student_name').html(window.localStorage.getItem("studfullname"));
                $('#student_name2').html(window.localStorage.getItem("studfullname"));
                $('#student_course').html(window.localStorage.getItem("COURSE"));
                $('#student_id').val(window.localStorage.getItem("IDNUMBER"));

                var len = decode.result.length;
                $("#list_subject").html('');
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.result[i];
                        var htmlData;

                        if (i % 2) {
                            if (row['highlight'] == true) {
                                htmlData = ' <li data-icon="false" class="subject-2">\
                                    <h2>' + row['subject'] + '</h2>\
                                    <p>' + row['faculty'] + '</p>\
                                    <input type="hidden" id="offercode" name="offercode" value="' + row['offercode'] + '">\
                                    <input type="hidden" id="subject" name="subject" value="' + row['subject'] + '">\
                                    <a href="#" data-offercode="' + row['offercode'] + '" data-subject="' + row['subject'] + '" class="aDeleteBtn ui-btn-up-done">EVALUATE</a>\
                                </li>';
                            } else {
                                htmlData = ' <li data-icon="false" class="subject-2">\
                                    <h2>' + row['subject'] + '</h2>\
                                    <p>' + row['faculty'] + '</p>\
                                    <input type="hidden" id="offercode" name="offercode" value="' + row['offercode'] + '">\
                                    <input type="hidden" id="subject" name="subject" value="' + row['subject'] + '">\
                                    <a href="#" data-offercode="' + row['offercode'] + '" data-subject="' + row['subject'] + '" class="aDeleteBtn ui-btn-up-r">EVALUATE</a>\
                                </li>';
                            }
                        } else {
                            if (row['highlight'] == true) {
                                htmlData = ' <li data-icon="false" class="subject-1">\
                                    <h2>' + row['subject'] + '</h2>\
                                    <p>' + row['faculty'] + '</p>\
                                    <input type="hidden" id="offercode" name="offercode" value="' + row['offercode'] + '">\
                                    <input type="hidden" id="subject" name="subject" value="' + row['subject'] + '">\
                                    <a href="#" data-offercode="' + row['offercode'] + '" data-subject="' + row['subject'] + '" class="aDeleteBtn ui-btn-up-done">EVALUATE</a>\
                                </li>';
                            } else {
                                htmlData = ' <li data-icon="false" class="subject-1">\
                                    <h2>' + row['subject'] + '</h2>\
                                    <p>' + row['faculty'] + '</p>\
                                    <input type="hidden" id="offercode" name="offercode" value="' + row['offercode'] + '">\
                                    <input type="hidden" id= "subject" name = "subject" value = "' + row['subject'] + '" > \
                                    <a href="#" data-offercode="' + row['offercode'] + '" data-subject="' + row['subject'] + '" class="aDeleteBtn ui-btn-up-r">EVALUATE</a>\
                                </li>';
                            }
                        }
                        $("#list_subject").append(htmlData);
                    }

                    var $the_ul = $('#list_subject');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.listview('refresh');
                    } else {
                        $the_ul.trigger('create');
                    }
                }
                $.mobile.loading("hide");
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            $.mobile.loading("hide");
        }
    });
}


$(document).on("click", ".aDeleteBtn", function() {

    var offercode = $(this).data('offercode');
    var subject = $(this).data('subject');

    $('#faculty_name').html("");
    $('#faculty_department').html("");

    $("#list_question").html('');
    $('#description').val('');

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'checker',
            offercode: offercode,
            subject: subject,
            myid: window.localStorage.getItem("myID"),
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.msg);
                return;
            } else {
                console.log(decode.ssub);
                window.localStorage.setItem("ssub", decode.ssub);
                window.localStorage.setItem("ocode", offercode);

                $.mobile.changePage("#evaluation", {
                    transition: "slide"
                }, true, true);

                $(document).on('pageshow', '#evaluation', function(event, data) {
                    get_instructor_information(window.localStorage.getItem("ocode"));
                });
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });
});

function get_instructor_information(offercode) {

    $.mobile.loading("show", {
        text: "Loading...",
        textonly: false,
        textVisible: true,
        theme: 'b'
    });

    console.log('hahhhahaha $.mobile.loading(show);');

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'faculty_info',
            offercode: offercode,
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.msg);
                return;
            } else {
                console.log(decode.instructor);
                window.localStorage.setItem("instructor_id", decode.instructor.idnumber);
                window.localStorage.setItem("instructor_fullname", decode.instructor.fullname);
                window.localStorage.setItem("instructor_department", decode.instructor.department);
                window.localStorage.setItem("instructor_picture", decode.instructor.picture);


                $('#faculty_name').html(window.localStorage.getItem("instructor_fullname"));
                $('#faculty_department').html(window.localStorage.getItem("instructor_department"));

                var img = window.localStorage.getItem("instructor_picture");
                var new_img;
                if (img === 'W0JJTkFSWV0=' || img === '') {
                    new_img = 'img/download.jpg';
                } else {
                    new_img = 'data:image/x-xbitmap;base64,' + img;
                }
                $('#faculty_pic').attr('src', new_img);

                $.mobile.loading('hide');
                console.log(' $.mobile.loading(hide);');

                generate_question();
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);

            $.mobile.loading("hide");
        }
    });
}


function generate_question() {
    $.mobile.loading('show');
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'generate_question',
            ter_scale: window.localStorage.getItem("ter_scale")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                console.log(decode.instructor);

                var len = decode.instructor.length;

                $("#list_question").html('');

                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.instructor[i];

                        var htmlData = '<li>\
                            <div class="ui-body ui-body-a ui-corner-all">\
                                <p><strong>' + row['Question'] + '</strong> <x style ="color:red;">' + row['isempty'] + '</x></p>\
                            </div>\
                            <form>\
                                <fieldset data-role="controlgroup" id="list_choices">\
                                    <input type="radio" name="' + row['Never_NAME'] + '" id="' + row['Never_ID'] + '" value="5">\
                                    <label for="' + row['Never_ID'] + '"><b>5</b></label>\
                                    <input type="radio" name="' + row['Seldom_NAME1'] + '" id="' + row['Seldom_ID1'] + '" value="6">\
                                    <label for="' + row['Seldom_ID1'] + '"><b>6</b></label>\
                                    <input type="radio" name="' + row['Seldom_NAME2'] + '" id="' + row['Seldom_ID2'] + '" value="7">\
                                    <label for="' + row['Seldom_ID2'] + '"><b>7</b></label>\
                                    <input type="radio" name="' + row['Often_NAME1'] + '" id="' + row['Often_ID1'] + '" value="8">\
                                    <label for="' + row['Often_ID1'] + '"><b>8</b></label>\
                                    <input type="radio" name="' + row['Often_NAME2'] + '" id="' + row['Often_ID2'] + '" value="9">\
                                    <label for="' + row['Often_ID2'] + '"><b>9</b></label>\
                                    <input type="radio" name="' + row['Always_NAME'] + '" id="' + row['Always_ID'] + '" value="10">\
                                    <label for="' + row['Always_ID'] + '"><b>10</b></label>\
                                </fieldset>\
                            </form>\
                        </li>';

                        $("#list_question").append(htmlData);
                    }

                    var $the_ul = $('#list_question');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.trigger('create');
                        $the_ul.listview('refresh');
                        console.log('$the_ul.listview(refresh)');
                    } else {
                        $the_ul.trigger('create');
                        console.log('$the_ul.trigger(create);');
                    }
                }
                $.mobile.loading('hide');
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);

            $.mobile.loading("hide");
        }
    });
}

function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    var empty = false;

    if ($('#description').val() == '') {
        alert('Evaluation comment is required.');
        empty = true;
        return false;
    }

    return true;
}

function submit_evaluation() {

    if (validate()) {

        $.mobile.loading('show');

        var selected_radio = new Array();

        var i = 1
        $('#list_question li').each(function(index) {

            $("input[name=" + i + "]").each(function(index) {
                if ($("input[type='radio']").is(':checked')) {
                    listview_el = new Object();
                    listview_el.id = i;
                    listview_el.value = $("input[name*=" + i + "]:checked").val() ? $("input[name*=" + i + "]:checked").val() : null;
                } else {
                    listview_el = new Object();
                    listview_el.id = i;
                    listview_el.value = null;
                }
            });
            selected_radio.push(listview_el);
            i++;
        });

        console.log(selected_radio);

        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
            async: false,
            type: 'POST',
            data: {
                command: 'save_evaluation',
                ocode: window.localStorage.getItem("ocode"),
                faculty_id: window.localStorage.getItem("instructor_id"),
                comments: $('#description').val(),
                currentuserid: window.localStorage.getItem("IDNUMBER"),
                myid: window.localStorage.getItem("myID"),
                ter_scale: window.localStorage.getItem("ter_scale"),
                data: selected_radio
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    alert(decode.msg);
                    $.mobile.loading("hide");
                    window.location.href = "profile.html"
                    return;
                } else {
                    $.mobile.loading("hide");
                    alert(decode.msg);
                    return;
                }

            },
            error: function(error) {
                console.log("Error:");
                console.log(error.responseText);
                $.mobile.loading("hide");
            }
        });
    }
}
