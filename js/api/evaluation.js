document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function() {
    onDeviceReady();
});

function onDeviceReady() {
    get_instructor_information(window.localStorage.getItem("ocode"));
}

function get_instructor_information(offercode) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        //        url: 'http://192.168.3.113/MyTer/TER_API/index.php',
        // url: 'http://localhost/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'faculty_info',
            offercode: offercode,
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.msg);
                return;
            } else {
                console.log(decode.instructor);
                window.localStorage.setItem("instructor_fullname", decode.instructor.fullname);
                window.localStorage.setItem("instructor_department", decode.instructor.department);
                window.localStorage.setItem("instructor_picture", decode.instructor.picture);

                $('#faculty_name').html(window.localStorage.getItem("instructor_fullname"));
                $('#faculty_department').html(window.localStorage.getItem("instructor_department"));

                generate_question();
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });
}


function generate_question() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        //        url: 'http://192.168.3.113/MyTer/TER_API/index.php',
        // url: 'http://localhost/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'generate_question'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                console.log(decode.instructor);

                var len = decode.instructor.length;

                $("#list_question").html('');

                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.instructor[i];

                        var htmlData = '<li>\
                            <div class="ui-body ui-body-a ui-corner-all">\
                                <p><strong>' + row['Question'] + '</strong> <x style ="color:red;">' + row['isempty'] + '</x></p>\
                            </div>\
                            <form>\
                                <fieldset data-role="controlgroup" id="list_choices">\
                                    <input type="radio" name="' + row['Never_NAME'] + '" id="' + row['Never_ID'] + '" value="Never">\
                                    <label for="' + row['Never_ID'] + '">5 [NEVER]</label>\
                                    <input type="radio" name="' + row['Seldom_NAME'] + '" id="' + row['Seldom_ID'] + '" value="Seldom">\
                                    <label for="' + row['Seldom_ID'] + '">6 [SELDOM]</label>\
                                    <input type="radio" name="' + row['Often_NAME'] + '" id="' + row['Often_ID'] + '" value="Often">\
                                    <label for="' + row['Often_ID'] + '">7</label>\
                                    <input type="radio" name="' + row['Always_NAME'] + '" id="' + row['Always_ID'] + '" value="Always">\
                                    <label for="' + row['Always_ID'] + '">8</label>\
                                </fieldset>\
                            </form>\
                        </li>';

                        $("#list_question").append(htmlData);
                    }

                    var $the_ul = $('#list_question');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.trigger('create');
                        $the_ul.listview('refresh');
                        console.log('$the_ul.listview(refresh)');
                    } else {
                        $the_ul.trigger('create');
                        console.log('$the_ul.trigger(create);');
                    }
                }

            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });
}

function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    var empty = false;

    if ($('#description').val() == '') {
        alert('Evaluation comment is required.');
        empty = true;
        return false;
    }

    return true;
}

function submit_evaluation() {

    if (validate()) {

        var selected_radio = new Array();

        var i = 1
        $('#list_question li').each(function(index) {
            // listview_el.id = $(this).find("input[name='" + i + "'] :radio:checked").val();
            $("input[name*=" + i + "]:checked").each(function() {
                listview_el = new Object();
                listview_el.id = $(this).val();
                selected_radio.push(listview_el);
            });
            i++;
        });

        console.log(selected_radio);

        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
            //            url: 'http://192.168.3.113/MyTer/TER_API/index.php',
            // url: 'http://localhost/MyTer/TER_API/index.php',
            async: false,
            type: 'POST',
            data: {
                command: 'save_evaluation',
                ocode: window.localStorage.getItem("ocode"),
                comments: $('#description').val(),
                currentuserid: window.localStorage.getItem("currentuserid"),
                myid: window.localStorage.getItem("myID"),
                1: $("input[name*='1']:checked").val(),
                2: $("input[name*='2']:checked").val(),
                3: $("input[name*='2']:checked").val(),
                4: $("input[name*='4']:checked").val(),
                5: $("input[name*='5']:checked").val(),
                6: $("input[name*='6']:checked").val(),
                7: $("input[name*='7']:checked").val(),
                8: $("input[name*='8']:checked").val(),
                9: $("input[name*='9']:checked").val(),
                10: $("input[name*='10']:checked").val(),
                11: $("input[name*='11']:checked").val(),
                12: $("input[name*='12']:checked").val(),
                13: $("input[name*='13']:checked").val(),
                14: $("input[name*='14']:checked").val(),
                15: $("input[name*='15']:checked").val(),
                16: $("input[name*='16']:checked").val(),
                17: $("input[name*='17']:checked").val(),
                18: $("input[name*='18']:checked").val(),
                19: $("input[name*='19']:checked").val(),
                20: $("input[name*='20']:checked").val()
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.href = "profile.html"
                    return;
                } else {
                    alert(decode.msg);
                    return;
                }
            },
            error: function(error) {
                console.log("Error:");
                console.log(error.responseText);
            }
        });
    }
}
