document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function() {
    onDeviceReady();
});

function onDeviceReady() {
    window.localStorage.clear();
    $('#ipadd').val(sessionStorage.getItem("ipaddress"));
}



function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#username').val() == '') {
        alert('Username is required.');
        empty = true;
        return false;
    }

    if ($('#password').val() == '') {
        alert('Password is required.');
        empty = true;
        return false;
    }

    if ($('#ipadd').val() == '') {
        alert('IP Address is required.');
        empty = true;
        return false;
    }

    sessionStorage.setItem("ipaddress", $('#ipadd').val());
    return true;
}


//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function get_year_sem_userid() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'settings'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.error);
                window.localStorage.clear();
            } else {

                window.localStorage.setItem("currentyear", decode.result.currentyear);
                window.localStorage.setItem("currentsemester", decode.result.currentsemester);
                window.localStorage.setItem("currentuserid", decode.result.currentuserid);

            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            alert("Fail AJAX communication");
            return;
        }
    });
}


function login() {

    if (validate() == true) {

        $.mobile.loading("show", {
            text: "Loading...",
            textonly: false,
            textVisible: true,
            theme: 'b'
        });

        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
            async: false,
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {
                command: 'login',
                username: $('#username').val(),
                password: $('#password').val(),
                usertype: $('#usertype').val()
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == false) {
                    $.mobile.loading("hide");
                    alert(decode.error);
                    return;
                } else {

                    console.log(decode.users);

                    if (decode.type == 'student') {

                        console.log('student');

                        window.localStorage.clear();

                        window.localStorage.setItem("myID", decode.users.myid);
                        window.localStorage.setItem("studfullname", decode.users.studfullname);
                        window.localStorage.setItem("initialnum", decode.users.initialnum);
                        window.localStorage.setItem("LASTNAME", decode.users.LASTNAME);
                        window.localStorage.setItem("FIRSTNAME", decode.users.FIRSTNAME);
                        window.localStorage.setItem("IDNUMBER", decode.users.IDNUMBER);
                        window.localStorage.setItem("COURSE", decode.users.COURSE);
                        window.localStorage.setItem("CURRICULUM", decode.users.CURRICULUM);
                        window.localStorage.setItem("GENDER", decode.users.GENDER);

                        window.localStorage.setItem("indicator", decode.users.indicator);
                        window.localStorage.setItem("indicator1", decode.users.indicator1);
                        window.localStorage.setItem("indicator2", decode.users.indicator2);
                        window.localStorage.setItem("indicator3", decode.users.indicator3);
                        window.localStorage.setItem("indicator4", decode.users.indicator4);
                        window.localStorage.setItem("ter_scale", decode.users.ter_scale);

                        get_year_sem_userid();

                        $.mobile.loading("hide");

                        window.location.href = "profile.html";

                    } else if (decode.type == 'faculty') {
                        window.localStorage.clear();

                        console.log('faculty');

                        window.localStorage.setItem("insidnumber", decode.users.IDNUMBER);
                        window.localStorage.setItem("instructorfullname", decode.users.instructorfullname);
                        window.localStorage.setItem("initialnum", decode.users.initialnum);
                        window.localStorage.setItem("LASTNAME", decode.users.LASTNAME);
                        window.localStorage.setItem("FIRSTNAME", decode.users.FIRSTNAME);
                        window.localStorage.setItem("IDNUMBER", decode.users.IDNUMBER);

                        window.localStorage.setItem("exclusiveforreports", '1');

                        window.localStorage.setItem("indicator", decode.users.indicator);
                        window.localStorage.setItem("indicator1", decode.users.indicator1);
                        window.localStorage.setItem("indicator2", decode.users.indicator2);
                        window.localStorage.setItem("indicator3", decode.users.indicator3);
                        window.localStorage.setItem("indicator4", decode.users.indicator4);
                        window.localStorage.setItem("ter_scale", decode.users.ter_scale);
                        window.localStorage.setItem("picture", decode.users.picture);

                        get_year_sem_userid();

                        $.mobile.loading("hide");

                        window.location.href = "profilepeer.html";

                    } else if (decode.type == 'dean') {
                        window.localStorage.clear();

                        console.log('dean');

                        window.localStorage.setItem("deanidnumber", decode.users.IDNUMBER);
                        window.localStorage.setItem("deanfullname", decode.users.deanfullname);
                        // window.localStorage.setItem("initialnum", decode.users.initialnum);
                        window.localStorage.setItem("LASTNAME", decode.users.LASTNAME);
                        window.localStorage.setItem("FIRSTNAME", decode.users.FIRSTNAME);
                        window.localStorage.setItem("IDNUMBER", decode.users.IDNUMBER);

                        window.localStorage.setItem("exclusiveforreports", '1');

                        window.localStorage.setItem("indicator", decode.users.indicator);
                        window.localStorage.setItem("indicator1", decode.users.indicator1);
                        window.localStorage.setItem("indicator2", decode.users.indicator2);
                        window.localStorage.setItem("indicator3", decode.users.indicator3);
                        window.localStorage.setItem("indicator4", decode.users.indicator4);
                        window.localStorage.setItem("ter_scale", decode.users.ter_scale);
                        window.localStorage.setItem("picture", decode.users.picture);

                        get_year_sem_userid();

                        $.mobile.loading("hide");

                        window.location.href = "profiledean.html";

                    } else if (decode.type == 'chairperson') {
                        window.localStorage.clear();

                        console.log('chairperson');

                        window.localStorage.setItem("chairmanidnumber", decode.users.IDNUMBER);
                        window.localStorage.setItem("chairmanfullname", decode.users.fullname);
                        // window.localStorage.setItem("initialnum", decode.users.initialnum);
                        window.localStorage.setItem("LASTNAME", decode.users.LASTNAME);
                        window.localStorage.setItem("FIRSTNAME", decode.users.FIRSTNAME);
                        window.localStorage.setItem("IDNUMBER", decode.users.IDNUMBER);

                        window.localStorage.setItem("exclusiveforreports", '1');

                        window.localStorage.setItem("indicator", decode.users.indicator);
                        window.localStorage.setItem("indicator1", decode.users.indicator1);
                        window.localStorage.setItem("indicator2", decode.users.indicator2);
                        window.localStorage.setItem("indicator3", decode.users.indicator3);
                        window.localStorage.setItem("indicator4", decode.users.indicator4);
                        window.localStorage.setItem("ter_scale", decode.users.ter_scale);
                        window.localStorage.setItem("picture", decode.users.picture);

                        get_year_sem_userid();

                        $.mobile.loading("hide");

                        window.location.href = "profilechairman.html";

                    }
                }
            },
            error: function(error) {
                console.log("Error:");
                console.log(error.responseText);
                console.log(error.message);

                $.mobile.loading("hide");

                alert("Fail AJAX communication");
                return;
            }
        });
    }
}
