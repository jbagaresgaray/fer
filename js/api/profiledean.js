document.addEventListener("deviceready", onDeviceReady, false);

$(document).ready(function() {
    console.log('Dean Profile');

    var redirect = false;
    var indicator = window.localStorage.getItem("indicator");
    var indicator1 = window.localStorage.getItem("indicator1");
    var indicator2 = window.localStorage.getItem("indicator2");
    var indicator3 = window.localStorage.getItem("indicator3");
    var indicator4 = window.localStorage.getItem("indicator4");

    if (indicator != '1') {
        redirect = true;
        console.log('indicator redirect = true');
    } else if (indicator1 != '1') {
        redirect = true;
        console.log('indicator1 redirect = true');
    } else if (indicator2 != '1') {
        redirect = true;
        console.log('indicator2 redirect = true');
    } else if (indicator3 != '1') {
        redirect = true;
        console.log('indicator3 redirect = true');
    } else if (indicator4 != '1') {
        redirect = true;
        console.log('indicator4 redirect = true');
    } else {
        redirect = false
        console.log('redirect = false');
    }

    // if (redirect) {
    //     window.location.href = "index.html";
    // } else {
    onDeviceReady();
    // }

});


function onDeviceReady() {

    $('#dean_name').html(window.localStorage.getItem("deanfullname"));
    $('#dean_name2').html(window.localStorage.getItem("deanfullname"));
    $('#dean_id').val(window.localStorage.getItem("IDNUMBER"));

    var img = window.localStorage.getItem("picture");
    var new_img;
    if (img === 'W0JJTkFSWV0=' || img === '') {
        new_img = 'img/download.jpg';
    } else {
        new_img = 'data:image/x-xbitmap;base64,' + img;
    }
    $('#dean_pic').attr('src', new_img);
    $('#dean_pic2').attr('src', new_img);

    getcollegebyid(window.localStorage.getItem("deanidnumber"));
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.html";
}

function getcollegebyid(idnumber) {

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        // url: 'http://localhost/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'getcollegebyid',
            idnumber: idnumber
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.error);
                window.localStorage.clear();
                return;
            } else {
                console.log(decode.college);
                window.localStorage.setItem("college", decode.college);
                $('#dean_department').html(window.localStorage.getItem("college"));

                getdepartments();
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });
}


function getdepartments() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'dean_departments',
            college: window.localStorage.getItem("college")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {

                return;
            } else {
                console.log(decode.department);
                var len = decode.department.length;
                $("#list_department").html('');
                $("#list_department2").html('');
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.department[i];
                        var htmlData;

                        if (i % 2) {
                            htmlData = '<li class="dean_dept2">\
                                    <a href="#"><h2>' + row + '</h2>\
                                    <input type="hidden" id="depname2" name="depname2" value="' + row + '">\
                                    </a>\
                                </li>';
                        } else {
                            htmlData = '<li class="dean_dept1">\
                                    <a href="#"><h2>' + row + '</h2>\
                                    <input type="hidden" id="depname2" name="depname2" value="' + row + '">\
                                    </a>\
                                </li>';
                        }
                        $("#list_department").append(htmlData);
                        $("#list_department2").append(htmlData);
                    }

                    var $the_ul = $('#list_department');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.listview('refresh');
                    } else {
                        $the_ul.trigger('create');
                    }

                    var $the_ul2 = $('#list_department2');
                    if ($the_ul2.hasClass('ui-listview')) {
                        $the_ul2.listview('refresh');
                    } else {
                        $the_ul2.trigger('create');
                    }
                }
            }

        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });

}


$(document).on("click", "#list_department li a", function() {
    var depname2 = $(this).closest("li").find('input[name="depname2"]').val();
    console.log('depname2 ' + depname2);

    window.localStorage.setItem("currentdep2", depname2);

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'dean_faculty',
            department: window.localStorage.getItem("currentdep2"),
            idnumber: window.localStorage.getItem("deanidnumber")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                console.log(decode.error);
                alert(decode.error);
                return;
            } else {
                console.log(decode.faculty);

                window.localStorage.setItem("chairman", JSON.stringify(decode.faculty));

                $.mobile.changePage("#instruction", {
                    transition: "slide"
                }, true, true);

                $(document).on('pageshow', '#instruction', function(event, data) {
                    console.log('instruction');

                    $('#faculty_profile_name').html(decode.faculty.fullname);
                    $('#instructor_profile_depart').html(decode.faculty.department);
                    $('#faculty_profile_id').val(decode.faculty.IDNUMBER);

                    var img = decode.faculty.picture;
                    var new_img;
                    if (img === 'W0JJTkFSWV0=' || img === '') {
                        new_img = 'img/download.jpg';
                    } else {
                        new_img = 'data:image/x-xbitmap;base64,' + img;
                    }
                    $('#faculty_profile_pic').attr('src', new_img);

                });
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
        }
    });

});

$(document).on("click", "#list_department2 li a", function() {
    var depname2 = $(this).closest("li").find('input[name="depname2"]').val();
    console.log('depname2 ' + depname2);

    window.localStorage.setItem("currentdep2", depname2);

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'getdepartmentfaculty',
            department: window.localStorage.getItem("currentdep2"),
            idnumber: window.localStorage.getItem("deanidnumber")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                return;
            } else {
                console.log(decode.faculty);

                var len = decode.faculty.length;
                $("#list_faculty2").html('');
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.faculty[i];
                        var htmlData;

                        if (i % 2) {
                            htmlData = '<li class="dean_dept2">\
                                    <a href="#"><h2>' + row['FULLNAME'] + '</h2>\
                                    <input type="hidden" id="facidnum" name="facidnum" value="' + row['FIDNUMBER'] + '">\
                                    </a>\
                                </li>';
                        } else {
                            htmlData = '<li class="dean_dept1">\
                                    <a href="#"><h2>' + row['FULLNAME'] + '</h2>\
                                    <input type="hidden" id="facidnum" name="facidnum" value="' + row['FIDNUMBER'] + '"a>\
                                    </a>\
                                </li>';
                        }
                        $("#list_faculty2").append(htmlData);
                    }

                    var $the_ul = $('#list_faculty2');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.listview('refresh');
                    } else {
                        $the_ul.trigger('create');
                    }

                    $.mobile.changePage("#faculty_report", {
                        transition: "slide"
                    }, true, true);
                }
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
        }
    });
});



$(document).on("click", "#btn-startevaluation", function() {
    var facidnum = JSON.parse(window.localStorage['chairman'] || '{}');
    var ipaddress = sessionStorage.getItem("ipaddress");

    console.log('ID NUMBER:' + facidnum.IDNUMBER);

    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'checkerchairman',
            faculty_id: facidnum.IDNUMBER,
            myid: window.localStorage.getItem("IDNUMBER"),
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.msg);
                return;
            } else {
                $.ajax({
                    url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
                    async: false,
                    type: 'POST',
                    data: {
                        command: 'getfacultybyid',
                        facidnum: facidnum.IDNUMBER
                    },
                    success: function(response) {
                        var decode = response;
                        console.log(decode);
                        if (decode.success == false) {
                            return;
                        } else {
                            console.log(decode.facultyname);

                            $.mobile.changePage("#faculty_profile", {
                                transition: "slide"

                            }, true, true);


                            $('#faculty_profile').on('pageshow', function(event, ui) {
                                $.mobile.loading("show", {
                                    text: "Loading...",
                                    textonly: false,
                                    textVisible: true,
                                    theme: 'b'
                                });

                                $('#faculty_name').html(facidnum.fullname);
                                $('#faculty_id').val(facidnum.IDNUMBER);
                                $('#instructor_depart').html(facidnum.department);

                                var img = facidnum.picture;
                                var new_img;
                                if (img === 'W0JJTkFSWV0=' || img === '') {
                                    new_img = 'img/download.jpg';
                                } else {
                                    new_img = 'data:image/x-xbitmap;base64,' + img;
                                }
                                $('#faculty_pic').attr('src', new_img);


                                generate_question();

                                $.mobile.loading('hide');
                            });


                        }

                    },
                    error: function(error) {
                        console.log("Error:");
                        console.log(error);
                    }
                });
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });

});


$(document).on("click", "#list_faculty2 li a", function() {
    var facidnum = $(this).closest("li").find('input[name="facidnum"]').val();
    window.localStorage.setItem("facidnum", facidnum);

    $('#faculty_name').html("");
    $('#faculty_id').val("");
    $("#list_question").html('');
    $('#faculty_pic').attr('src', 'img/download.jpg');

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'getfacultybyid',
            facidnum: window.localStorage.getItem("facidnum")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                return;
            } else {
                console.log(decode.facultyname);

                $.mobile.changePage("#faculty_report_detail", {
                    transition: "slide"
                }, true, true);


                $('#faculty_report_detail').on('pageshow', function(event, ui) {
                    console.log('faculty_report_detail');

                    $.mobile.loading("show", {
                        text: "Loading...",
                        textonly: false,
                        textVisible: true,
                        theme: 'b'
                    });

                    $('#faculty_report_name').html(decode.facultyname.fullname);
                    $('#faculty_report_id').val(window.localStorage.getItem("facidnum"));

                    var img = decode.facultyname.picture;
                    var new_img;
                    if (img === 'W0JJTkFSWV0=' || img === '') {
                        new_img = 'img/download.jpg';
                    } else {
                        new_img = 'data:image/x-xbitmap;base64,' + img;
                    }
                    $('#faculty_report_pic').attr('src', new_img);

                    get_instructor_subjects();

                    $.mobile.loading('hide');
                });
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
        }
    });
});

function get_instructor_subjects() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'get_instructor_subjects',
            insidnumber: window.localStorage.getItem("facidnum"),
            ter_scale: window.localStorage.getItem("ter_scale")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == false) {
                alert(decode.msg);
                return;
            } else {
                console.log(decode.subjects);
                var len = decode.subjects.length;
                $('#table-column-toggle tbody').append('');
                $('#table-column-toggle tr:not(:first)').remove();
                if (len > 0) {;
                    for (var i = 0; i < len; i++) {

                        var data = decode.subjects[i];

                        var row = '<tr>';
                        row += '<th>' + data.subject + '</th>';
                        row += '<td>' + data.desc + '</td>';
                        row += '<td>0</td>';
                        row += '<td><a class="ui-btn ui-mini ui-btn-e clsReport">Report</a></td>';
                        $("#table-column-toggle tbody").append(row);
                    }
                }
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
        }
    });
}

function generate_question() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
        async: false,
        type: 'POST',
        data: {
            command: 'generate_question_chairman',
            ter_scale: window.localStorage.getItem("ter_scale")
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                console.log(decode.instructor);

                var len = decode.instructor.length;

                $("#list_question").html('');

                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var row = decode.instructor[i];

                        var htmlData = '<li>\
                            <div class="ui-body ui-body-a ui-corner-all">\
                                <p><strong>' + row['Question'] + '</strong> <x style ="color:red;">' + row['isempty'] + '</x></p>\
                            </div>\
                            <form>\
                                <fieldset data-role="controlgroup" id="list_choices">\
                                    <input type="radio" name="' + row['Never_NAME'] + '" id="' + row['Never_ID'] + '" value="5">\
                                    <label for="' + row['Never_ID'] + '"><b>5</b></label>\
                                    <input type="radio" name="' + row['Seldom_NAME1'] + '" id="' + row['Seldom_ID1'] + '" value="6">\
                                    <label for="' + row['Seldom_ID1'] + '"><b>6 </b></label>\
                                    <input type="radio" name="' + row['Seldom_NAME2'] + '" id="' + row['Seldom_ID2'] + '" value="7">\
                                    <label for="' + row['Seldom_ID2'] + '"><b>7</b></label>\
                                    <input type="radio" name="' + row['Often_NAME1'] + '" id="' + row['Often_ID1'] + '" value="8">\
                                    <label for="' + row['Often_ID1'] + '"><b>8</b></label>\
                                    <input type="radio" name="' + row['Often_NAME2'] + '" id="' + row['Often_ID2'] + '" value="9">\
                                    <label for="' + row['Often_ID2'] + '"><b>9</b></label>\
                                    <input type="radio" name="' + row['Always_NAME'] + '" id="' + row['Always_ID'] + '" value="10">\
                                    <label for="' + row['Always_ID'] + '"><b>10</b></label>\
                                </fieldset>\
                            </form>\
                        </li>';

                        $("#list_question").append(htmlData);
                    }

                    var $the_ul = $('#list_question');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.trigger('create');
                        $the_ul.listview('refresh');
                        console.log('$the_ul.listview(refresh)');
                    } else {
                        $the_ul.trigger('create');
                        console.log('$the_ul.trigger(create);');
                    }
                }

            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
        }
    });
}









function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    var empty = false;

    if ($('#description').val() == '') {
        alert('Evaluation comment is required.');
        empty = true;
        return false;
    }

    return true;
}


function submit_evaluation() {

    if (validate()) {

        $.mobile.loading('show');

        var selected_radio = new Array();

        var i = 1
        $('#list_question li').each(function(index) {

            $("input[name=" + i + "]").each(function(index) {
                if ($("input[type='radio']").is(':checked')) {
                    listview_el = new Object();
                    listview_el.id = i;
                    listview_el.value = $("input[name*=" + i + "]:checked").val() ? $("input[name*=" + i + "]:checked").val() : null;
                } else {
                    listview_el = new Object();
                    listview_el.id = i;
                    listview_el.value = null;
                }
            });
            selected_radio.push(listview_el);
            i++;
        });

        console.log(selected_radio);

        var facidnum = JSON.parse(window.localStorage['chairman'] || '{}');

        var ipaddress = sessionStorage.getItem("ipaddress");
        $.ajax({
            url: 'http://' + ipaddress + '/MyTer/TER_API/index.php',
            async: false,
            type: 'POST',
            data: {
                command: 'submit_evaluation_chairman',
                faculty_id: facidnum.IDNUMBER,
                comments: '',
                currentuserid: window.localStorage.getItem("IDNUMBER"),
                myid: window.localStorage.getItem("IDNUMBER"),
                ter_scale: window.localStorage.getItem("ter_scale"),
                data: selected_radio
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    alert(decode.msg);
                    $.mobile.loading("hide");
                    window.location.href = "profiledean.html"
                    return;
                } else {
                    $.mobile.loading("hide");
                    alert(decode.msg);
                    return;
                }

            },
            error: function(error) {
                console.log("Error:");
                console.log(error);
                $.mobile.loading("hide");
            }
        });
    }
}


$(document).on("click", ".clsReport", function() {
    $.mobile.changePage("reports_dean.html", {
        transition: "slide"
    }, true, true);
});
